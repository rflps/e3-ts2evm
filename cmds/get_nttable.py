#!/usr/bin/python3

import queue
import sys
import itertools

# Import p4p client context
from p4p.client.thread import Context


def main():

    # Check for at least one PV as input argument
    if (len(sys.argv)) < 2:
        print("[ERROR] No PV name specified.")
        return

    # Create PVA context
    ctxt = Context("pva", nt=False)

    try:
        # Take only the first PV name and try to get from the network
        evmTable = ctxt.get(str(sys.argv[1]), timeout=3, throw=True)

        if evmTable.has("evm_sequencer"):

            # Print a simple list of the content
            pad = 20
            print(str('').ljust(3*pad,'-'))
            print(str('EVENT').ljust(pad), str('CODE').ljust(pad), str('TIMESTAMP').ljust(pad))

            table_ = evmTable.evm_sequencer
            for (names, codes, ts) in zip(table_.EventName, table_.EventCode, table_.Timestamp):
                print(str(names).ljust(pad), str(codes).ljust(pad), str(ts).ljust(pad))
            print(str('').ljust(3*pad,'-'))
        else:
            print("[ERROR] PV", sys.argv[1], "does not have evm_sequencer field")
    except (queue.Empty, TimeoutError):
        print("[ERROR] Unable to connect to PV", sys.argv[1])

    ctxt.close()
    return


if __name__ == "__main__":
    main()
