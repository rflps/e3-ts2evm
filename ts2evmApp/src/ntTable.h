#ifndef _NTTABLE_H_
#define _NTTABLE_H_

#include <vector>
#include <string>

#include <pv/pvDatabase.h>
#include <pv/timeStamp.h>
#include <pv/pvTimeStamp.h>

#include <shareLib.h>

class ntTableEvmRecord;
typedef std::tr1::shared_ptr<ntTableEvmRecord> ntTableEvmRecordPtr;

// ntTableEvm class definition, inherited from PVRecord class
class epicsShareClass ntTableEvmRecord :
    public epics::pvDatabase::PVRecord
{
public:
    POINTER_DEFINITIONS(ntTableEvmRecord);
    static ntTableEvmRecordPtr create(std::string const & recordName);
    virtual void process();

    virtual ~ntTableEvmRecord() {}
    virtual bool init() {return false;}
    void updateTable();

private:
    ntTableEvmRecord(std::string const & recordName,
        epics::pvData::PVStructurePtr const & pvStructure);

};

#endif  /* _NTTABLE_H_ */
