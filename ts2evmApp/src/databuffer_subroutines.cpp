
/**
 * @file databuffer_subroutines.cpp
 * @author Joao Paulo Martins (joaopaulo.martins@ess.eu)
 * @brief Routine that writes to EVG data buffer
 * @version 0.1
 * @date 2020-07-17
 * 
 * @copyright Copyright (c) ESS 2020
 * 
 * The code below is based on supercygleEngine project:
 * https://github.com/icshwi/supercycleengine
 * 
 */

#include <stdio.h>
#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <epicsTypes.h>
#include <cmath>
#include <cstring>

#define EPICS2020s 946708560
#define STDPERIOD  71428
#define DBUF_SIZE  32 

static epicsUInt64 cycleId;
static epicsUInt8  databuffer[DBUF_SIZE]; // data buffer of 32 bytes

//------------------------------------------------------------------------
//  Initiate aSub record; 
//------------------------------------------------------------------------
static long cycleIdInit()
{
    // Copied from supercycleEngine module
    epicsTimeStamp ts;
    epicsTimeGetCurrent(&ts);   
    cycleId = round((ts.secPastEpoch - EPICS2020s) / STDPERIOD  * 1000000);
    std::memset(databuffer, 0, sizeof(databuffer)); 
    return 0;
}

//------------------------------------------------------------------------
//  aSub process to write data buffer
//------------------------------------------------------------------------
static long dataBufProcess(aSubRecord *prec)
{
    //offset 4: cycle Id (dbus-u32indx1-subA_)
    epicsUInt64 *pcycleid = (epicsUInt64 *) &databuffer[4];
    *pcycleid = cycleId;

    //offset 16: RF pulse length (dbus-u32indx4-subA_ /  PBLen)
    epicsUInt32 *prflen = (epicsUInt32 *) &databuffer[16];
    *prflen = *(epicsUInt32 *)prec->a;

    //offset 20: RF ROI Delay (dbus-u32indx5-subA_ / PBEn)
    epicsUInt32 *roidelay = (epicsUInt32 *) &databuffer[20];
    *roidelay = *(epicsUInt32 *)prec->b;

    //offset 24: RF ROI Size (dbus-u32indx6-subA_ / PBCurr)
    epicsUInt32 *roisize = (epicsUInt32 *) &databuffer[24];
    *roisize = *(epicsUInt32 *)prec->c;

    // Copy databuffer to output
    memcpy(prec->vala, databuffer, DBUF_SIZE);
    prec->neva = DBUF_SIZE / sizeof(epicsInt32); // TODO: check this! (FTVL = ULONG)

    //cycleID readback PV process
    memcpy(prec->valb, &cycleId, sizeof(epicsUInt64));
    prec->nevb = 1; // ?

    // Increase cycle ID
    cycleId++;

    return 0;
}

extern "C"
{
    epicsRegisterFunction(cycleIdInit);
    epicsRegisterFunction(dataBufProcess);
}
